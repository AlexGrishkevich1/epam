<?php
function php_number_prime() {
	// Your code.
	$result = [];
	for ($i = 0; $i<=10000; $i++) {
		$isPrime = 0;
		if ($i === 1) continue;
		$floorSqrJ = floor(sqrt($i));
		for ($j = 2; $j <= $floorSqrJ; $j++){ 
			if ($i % $j == 0) {
				$isPrime = 0;
				break;
			} 
			$isPrime = 1;
		} 
		if ($isPrime === 1) $result[] = $i;
	}
	return $result;
}

var_dump(php_number_prime());