<?php
function calcByOperator($args, $operator) {
	$result = 0;
	if (!in_array($operator, ["+", "-", "*", "/"])) 
	{return 0;}
	foreach($args as $key => $item) {
		if (floatval($item) === 0 || (strval(floatval($item)) !==strval($item))) continue;
		if ($key == 1) {
			$result += floatval($item);
		} elseif ($key > 1) {
			//eval("$result ".$operator."= floatval($item)");
			switch($operator) {
				case '+':
					$result += floatval($item);
					break;
				case '-':
					$result -= floatval($item);
					break;
				case '*':
					$result *= floatval($item);
					break;
				case '/':
					$result /= floatval($item);
					break;
				default:
					return $result = 0;
			}
		}
	}
	return $result;
}

function php_dynamic_php_dynamic_calculator()
{
	// Your code.
	$args = func_get_args();
	
	$operator = reset($args);
	$result = calcByOperator($args, $operator);
	return $result;
}

$operator = '+';
echo $result = php_dynamic_php_dynamic_calculator($operator, 1, '23', 'xd'); // Result: 24.
$operator = '*';
echo $result = php_dynamic_php_dynamic_calculator($operator, 2, 'hg', 2, 3); // Result: 12.
$operator = '-';
echo $result = php_dynamic_php_dynamic_calculator($operator, '2', '5', '5'); // Result: -8.
$operator = '/';
echo $result = php_dynamic_php_dynamic_calculator($operator, 1, 4, 'test'); // Result: 0.25.
$operator = 3;
echo $result = php_dynamic_php_dynamic_calculator(0, 16, 4, 2); // Result: 0.