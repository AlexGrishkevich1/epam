<?php
class ParedTag extends Tag{
	public $text;
	public function setText($text) {
		$this->text = $text;
		return $this;
	}

	public function generateTag() {
		return $this->answer.">".$this->text."</".$this->tagName.">";
	}
}

class SingleTag extends Tag{
	public function setText($text) {
		return $this;
	}

	public function generateTag() {
		return $this->answer.">";
	}
}

abstract class Tag {
	public $tagName;
	public $answer = "";
	public $attributes = [];
	public static function initialize($className, $tagname) {
		$tag = new $className;
		$tag->tagName = $tagname;
		$tag->answer.="<".$tagname." ";
		return $tag;
	}
	
	public function getTagName() {
		return $this->tagName;
	}
	
	public function setAttribute(string $name, $value) {
		$this->attributes[$name] = $value;
		$this->answer = $this->answer." ".$name."='".$value."'";
		return $this;
	}
	public function getAttributes() {
		return $this->attributes;
	}
}

echo Tag::initialize('ParedTag', 'a')->setText('tes')->setAttribute('href', 'vk.com')->generateTag();