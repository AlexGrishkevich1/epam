<?php
interface HomoSapiens {
	
}

interface Human extends HomoSapiens{
	public function gender(); 
	public function name();
}

abstract class Men implements Human{
	public function gender() {
		return 'men';
	}
}

class John extends Men {
	public function name() {
		return get_class($this);
	}
}

$john = new John();
$john->gender(); // men
$hs = $john instanceof HomoSapiens; // true
$h = $john instanceof HomoSapiens;
